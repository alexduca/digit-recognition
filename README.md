# Digit Recognition

This is my personal attempt at creating a neural network capable of recognizing digits. I am using [PyTorch](https://pytorch.org/) as a machine learning library and the [MNIST database of handwritten digits](http://yann.lecun.com/exdb/mnist/).

# Design
There are many sophisticated designs for such a neural network and even ways to create a bigger dataset by distorting the original one. I will refrain from all of that for now and stick with a design that is easy to understand and implement. [Simard et al.](http://ce.sharif.edu/courses/85-86/2/ce667/resources/root/15%20-%20Convolutional%20N.%20N./fugu9.pdf) achieved an error rate of 1,6% by doing the following:

- The neural network has two layers with 800 hidden units. The initial weights are random values drawn from the normal distribution with a standard deviation of (0.5).
- The loss function is cross-entropy.
- The optimizer is Stochastic Gradient Descent with an initial learning rate of (0.005). Every 100 epochs the learning rate is multiplied by (0.3).

I also had to make some assumptions about a few other implementation details:

- ReLU ist used as an activation function after the first layer.
- No mini-batches were used.

The algorithm for training the network can be found in `training.py`.

# Result

The trained network achieved an error rate of 3.1% and is saved as `trained_network_1.py`. I am sure that my implementation is slightly different from the one in the paper above, which caused the slightly worse error rate. The following list of statistics shows which digits were harder to classify:

- error rate for digit 0 is 1.2 %
- error rate for digit 1 is 1.1 %
- error rate for digit 2 is 4.6 %
- error rate for digit 3 is 3.2 %
- error rate for digit 4 is 3.0 %
- error rate for digit 5 is 3.3 %
- error rate for digit 6 is 2.9 %
- error rate for digit 7 is 3.2 %
- error rate for digit 8 is 4.0 %
- error rate for digit 9 is 4.4 %

The following collage shows every wrongly classified image:

![wrongly classified images](https://gitlab.com/alexduca/digit-recognition/-/raw/main/wrongly_classified_images_1.png)
